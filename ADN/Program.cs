﻿/*
 * Projet : ADN
 * Auteur : Luca
 * Détail : calcul du nombre d'occurence des bases ACGT
 * Date : 14.10.2022
 * Version : 1.0
 */

using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;

namespace ADN
{
    internal class Program
    {
        static void Main(string[] args)
        {
    
            StreamReader s;
            s = new StreamReader("C:\\Users\\Luca\\Documents\\2022-23\\OutilDeveloppeur\\chromosome-11-partial.txt");
            String line;
            line = s.ReadLine();

            int compterA = 0;
            int compterT = 0;
            int compterG = 0;
            int compterC = 0;




            while (line != null)
            {
                //Console.WriteLine(Line);
                //Console.WriteLine(line[0]);


                for (int i = 0; i < line.Length; i = i + 1)
                {
                    if (line[0] == 'A')
                    {
                        compterA++;
                    }
                    if (line[0] == 'T')
                    {
                        compterT++;
                    }
                    if (line[0] == 'G')
                    {
                        compterG++;
                    }
                    if (line[0] == 'C')
                    {
                        compterC++;
                    }
                }
                line = s.ReadLine();
            }
            Console.WriteLine("compter A =" + compterA);
            Console.WriteLine("compter T =" + compterT);
            Console.WriteLine("compter G =" + compterG);
            Console.WriteLine("compter C =" + compterC);
            Console.ReadKey();

        }
    }
}
